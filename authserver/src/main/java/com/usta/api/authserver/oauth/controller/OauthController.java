package com.usta.api.authserver.oauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Map;

@RestController
class OauthController {

    @Autowired
    private TokenEndpoint tokenEndpoint;

    @GetMapping("/auth/token")
    public ResponseEntity<OAuth2AccessToken> getAccessToken(Principal principal, @RequestParam
            Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {
        return postAccessToken(principal, parameters);
    }

    @PostMapping("/auth/token")
    public ResponseEntity<OAuth2AccessToken> postAccessToken(Principal principal, @RequestParam Map<String, String> parameters) throws HttpRequestMethodNotSupportedException {

        String grantTypeKey = "grant_type";
        String grantTypeValue = parameters.get(grantTypeKey);
        parameters.put(grantTypeKey, grantTypeValue.toLowerCase());

        return tokenEndpoint.postAccessToken(principal, parameters);
    }

    private String getParam(Map<String, String> parameters, String paramKey) throws Exception {
        if (!parameters.containsKey(paramKey)) {
            throw new Exception("No " + paramKey + " provided");
        }
        return parameters.get(paramKey);
    }

}