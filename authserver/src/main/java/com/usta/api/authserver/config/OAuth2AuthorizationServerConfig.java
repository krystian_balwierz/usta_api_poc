package com.usta.api.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.util.Arrays;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    ClientDetailsService clientDetailsService;

    @Autowired
    AuthorizationServerTokenServices tokenServices;


    @Override
    public void configure(final AuthorizationServerSecurityConfigurer security) {
        security.allowFormAuthenticationForClients();
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient("d906cc61c4724aa28015dd775ce6c9a7")
                .secret("{noop}3181c3bbacad4460806B1DAF94D33827")
                .authorizedGrantTypes("client_credentials")
                .scopes("READ", "CUSTOMER_INSERT")
                .and()
                .withClient("06f1441bb3574ccc9375f0bcb1495475")
                .secret("{noop}20df39e8f0464750A04AF6B61AF4475C")
                .authorizedGrantTypes("client_credentials")
                .scopes("WRITE", "READ", "UPDATE", "WRITE_PROFILE")
                .and()
                .withClient("f5c9bd84c8744eb7af3c668e69a0b74d")
                .secret("{noop}C1030D17c9d94007992Cf99830e1A007")
                .authorizedGrantTypes("client_credentials")
                .scopes("READ_DATA");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(accessTokenConverter()));

        endpoints.tokenStore(tokenStore())
                .tokenEnhancer(tokenEnhancerChain)
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
                .pathMapping("/oauth/token","/auth/token");

    }

    JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
                new ClassPathResource("usta_api.jks"), "tennis".toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("usta"));
        return converter;
    }

    TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

}