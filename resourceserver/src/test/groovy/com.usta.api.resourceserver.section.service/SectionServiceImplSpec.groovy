package com.usta.api.resourceserver.hybris.section.service

import com.usta.api.resourceserver.section.model.SectionResponse
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

import static java.util.Arrays.asList

class SectionServiceImplSpec extends Specification {

    SectionServiceImpl sectionService

    RestTemplate restTemplate

    String urlToGetSection

    def setup() {
        restTemplate = Mock(RestTemplate)

        urlToGetSection = "ustacommercewebservices/v2/usta/section/getSection"

        sectionService = new SectionServiceImpl(restTemplate, urlToGetSection)
    }

    def 'given empty accessToken when doRequest() then exception'() {
        given:
        def accessToken = ""
        def emptyMap = new LinkedMultiValueMap<String, String>()

        when:
        sectionService.doRequest(accessToken, emptyMap)

        then:
        thrown(MissingServletRequestParameterException)
    }

    def 'given some accessToken when doRequest() then exception'() {
        given:
        def accessToken = "12qdasa"
        def emptyMap = new LinkedMultiValueMap<String, String>()

        def mockedResponse = new SectionResponse(asList("Czikago", "Solina"))

        URI uri = sectionService.getUriToGetSection(emptyMap)

        when:
        def response = sectionService.doRequest(accessToken, emptyMap)

        then:
        restTemplate.getForObject(uri, SectionResponse) >> mockedResponse

        response == mockedResponse
    }
}
