package com.usta.api.resourceserver.hybris.section.service;

import com.usta.api.resourceserver.hybris.section.model.SectionResponse;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class SectionServiceImpl implements SectionService {
    
    private String urlGetSection;
    
    private RestTemplate restTemplate;
    
    public SectionServiceImpl(@Qualifier("janRainRestTemplate") RestTemplate restTemplate,
                              @Value("${section.url}") String urlGetSection) {
        this.restTemplate = restTemplate;
        this.urlGetSection = urlGetSection;
    }
    
    //todo how request model/class should look like. AccessToken as Field ? Or this map is enough
    @Override
    public SectionResponse doRequest(final String accessToken, MultiValueMap<String, String> parameters)
        throws MissingServletRequestParameterException {
        checkAccessTokenIsNotEmpty(accessToken);
        
        URI uriToGetSection = getUriToGetSection(parameters);
        System.out.println(uriToGetSection.toString());
        SectionResponse response = restTemplate.getForObject(uriToGetSection, SectionResponse.class);
        
        return response;
    }
    
    private void checkAccessTokenIsNotEmpty(String accessToken) throws MissingServletRequestParameterException {
        if (accessToken == null || accessToken.isEmpty()) {
            throw new MissingServletRequestParameterException("access_token", "String");
        }
    }
    
    URI getUriToGetSection(MultiValueMap<String, String> parameters) {
        return UriComponentsBuilder.fromUriString(urlGetSection)
                                   .queryParams(parameters)
                                   .build()
                                   .toUri();
    }
    
}
