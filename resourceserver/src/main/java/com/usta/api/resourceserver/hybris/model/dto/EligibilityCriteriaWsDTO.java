package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class EligibilityCriteriaWsDTO implements java.io.Serializable {


    private String typeCode;


    private String typeName;


    private String dataTypeCode;


    private String dataTypeName;


    private String name;


    private String description;


    private Date minDate;


    private Date maxDate;


    private Double minRange;


    private Double maxRange;


    private String value;


    private Boolean booleanValue;


    private String status;


    private EligibleCrTypeWsDTO type;


    private EligibleCrDataTypeWsDTO dataType;
}
