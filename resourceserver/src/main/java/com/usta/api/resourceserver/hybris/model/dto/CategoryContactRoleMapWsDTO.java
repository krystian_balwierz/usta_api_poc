package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class CategoryContactRoleMapWsDTO implements java.io.Serializable {


    private String status;


    private Date startDate;


    private Date endDate;


    private USTARoleWsDTO role;


    private ContactPersonWsDTO customer;


    private EventCategoryWsDTO eventCategory;
}
