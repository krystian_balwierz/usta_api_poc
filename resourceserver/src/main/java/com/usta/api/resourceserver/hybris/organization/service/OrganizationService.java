package com.usta.api.resourceserver.hybris.organization.service;

import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;

public interface OrganizationService {

    GetOrganizationResponse getOrganizationByEmail(String hybrisAccessToken, String emailId);

    GetOrganizationResponse getOrganizationByUid(String hybrisAccessToken, String organizationId);
}
