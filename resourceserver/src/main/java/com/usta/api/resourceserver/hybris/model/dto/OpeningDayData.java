package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class OpeningDayData implements java.io.Serializable {


    private TimeData openingTime;


    private TimeData closingTime;
}
