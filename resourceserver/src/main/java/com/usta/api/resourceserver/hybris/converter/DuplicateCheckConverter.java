package com.usta.api.resourceserver.hybris.converter;

import com.usta.api.resourceserver.hybris.model.DupCheckData;
import com.usta.api.resourceserver.hybris.model.DupCheckRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DuplicateCheckConverter implements Converter<DupCheckData, DupCheckRequest> {

    @Override
    public DupCheckRequest convert(DupCheckData source) {
        DupCheckRequest dupCheckRequest = new DupCheckRequest();
        dupCheckRequest.setDateOfBirth(source.getDateOfBirth()+"T00:00:00-0500");
        dupCheckRequest.setFirstName(source.getFirstName());
        dupCheckRequest.setLastName(source.getLastName());
        dupCheckRequest.setUaid(source.getUaid());
        //FIXME
        dupCheckRequest.setLightWeightAccount("true");
        return dupCheckRequest;
    }
}
