package com.usta.api.resourceserver.login.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@JsonAutoDetect
public class LoginData {
    String uuid;
    @JsonProperty("access_token")
    String accessToken;
    String sourceSystem;
    String dateOfBirth;
    String gender;
    String postalCode;
}
