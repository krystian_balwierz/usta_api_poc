package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class CourtSizeWsDTO implements java.io.Serializable {


    private String code;


    private String name;


    private Boolean isLined;


    private String description;
}
