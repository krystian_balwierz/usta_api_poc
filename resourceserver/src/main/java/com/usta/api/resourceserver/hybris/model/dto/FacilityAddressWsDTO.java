package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

@Data
public class FacilityAddressWsDTO implements java.io.Serializable {


    private String title;


    private String titleCode;


    private String firstName;


    private String lastName;


    private String companyName;


    private String line1;


    private String line2;


    private String town;


    private String postalCode;


    private String phone;


    private String email;


    private Boolean shippingAddress;


    private Boolean visibleInAddressBook;


    private String formattedAddress;


    private String taLine3;


    private String taCareOf;


    private String taStatus;


    private String taAddressType;


    private RegionWsDTO region;


    private CountryWsDTO country;


    private CountyWsDTO taCounty;


    private USTASectionWsDTO taSection;


    private USTADistrictWsDTO taDistrict;
}
