package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class WeekdayOpeningDayDataWsDTO implements java.io.Serializable {


    private String weekDay;


    private boolean closed;


    private TimeData openingTime;


    private TimeData closingTime;

}
