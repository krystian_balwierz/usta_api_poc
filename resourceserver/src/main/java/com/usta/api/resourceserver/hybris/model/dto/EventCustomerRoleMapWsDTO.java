package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class EventCustomerRoleMapWsDTO implements java.io.Serializable {


    private USTARoleWsDTO role;


    private Date startDate;


    private Date endDate;


    private String status;


    private String customerName;


    private String customerPhone;


    private String customerEmail;
}
