package com.usta.api.resourceserver.hybris.model;


import lombok.Data;

@Data
public class DupCheckData {
    String uaid;
    String firstName;
    String lastName;
    String dateOfBirth;
    String gender;
}
