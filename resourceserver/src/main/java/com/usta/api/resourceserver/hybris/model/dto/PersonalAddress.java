package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class PersonalAddress {
    private String postalcode;
}
