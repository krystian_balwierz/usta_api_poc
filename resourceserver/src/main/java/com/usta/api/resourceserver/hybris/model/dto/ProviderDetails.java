package com.usta.api.resourceserver.hybris.model.dto;

import com.usta.api.resourceserver.hybris.model.JanRainName;
import lombok.Data;

@Data
public class ProviderDetails {
    private String verifiedEmail;
    private JanRainName name;
    private String displayName;
    private String dateOfBirth;
    private String gender;
    private PersonalAddress personalAddress;
}
	