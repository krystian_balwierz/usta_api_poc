package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class USTADistrictWsDTO implements java.io.Serializable {


    private String code;


    private String name;


    private String sectionCode;
}
