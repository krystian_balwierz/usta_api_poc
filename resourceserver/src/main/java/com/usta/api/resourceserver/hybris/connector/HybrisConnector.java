package com.usta.api.resourceserver.hybris.connector;

import com.usta.api.resourceserver.hybris.model.DupCheckRequest;
import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;
import com.usta.api.resourceserver.hybris.model.HybrisTokenResponse;
import com.usta.api.resourceserver.hybris.model.InsertRequest;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;

public interface HybrisConnector {
    UserUpdateWsDTO insertCustomerDetails(InsertRequest insertRequest);

    HybrisTokenResponse getHybrisToken(String email);

    GetOrganizationResponse getOrganization(String hybrisAccessToken, String emailId, String organizationId);

    String customerDupeCheck(DupCheckRequest dupCheckRequest);
}
