package com.usta.api.resourceserver.hybris.datapump.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DatapumpController {
    @GetMapping("/retrieveAllCustomerDetails")
    public void retrieveAllCustomerDetails() {
        /*
        In Hybris:

        	@RequestMapping(value = "/retriveAllCustomerDetails", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public USTACustomerListWsDTO retriveCustomerDetailsForDataPump(@RequestParam(required = false, defaultValue = "0") int limit,
			@RequestParam(required = false) String date, @RequestParam(required = false) String uaid)


    In Mule - check usta_maven.xml, be careful: com.usta.integration.transformer.DataPumpModifiedTime
         */

    }

    @GetMapping("/retrieveAllOrganizationDetails")
    public void retrieveAllOrganizationDetails() {
        /*
        In Hybris:

        @RequestMapping(value = "/retriveAllOrganizationDetails", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public USTAOrganizationListWsDTO retriveOrganizationDetailsForDataPump(
			@RequestParam(required = false, defaultValue = "0") int limit, @RequestParam(required = false) String date,
			@RequestParam(required = false) String uaid)

	In Mule - check usta_maven.xml, be careful: com.usta.integration.transformer.DataPumpModifiedTime
         */
    }

}
