package com.usta.api.resourceserver.mail.service;

import com.usta.api.resourceserver.mail.exception.CheetachException;
import com.usta.api.resourceserver.mail.parameters.MailEntity;

public interface CheetachMailService {
    
    void loginMail() throws CheetachException;
    
    void triggerMail(final MailEntity mailEntity) throws CheetachException;
    
    default void checkResponseIsOk(String response, String failMessage) throws CheetachException {
        if (!response.trim()
                     .equals("OK")) {
            throw new CheetachException(failMessage + " response: " + response);
        }
    }
    
}
