package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class TimeData implements java.io.Serializable {


    private byte hour;


    private byte minute;


    private String formattedHour;

}
