package com.usta.api.resourceserver.janrain.service;

import com.usta.api.resourceserver.janrain.exception.JanRainTokenValidationException;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;

public interface JanRainService {
    JanRainUserData validateToken(String uuid, String accessToken) throws JanRainTokenValidationException;
}
