package com.usta.api.resourceserver.login.service;

import com.usta.api.resourceserver.exception.MuleLikeException;
import com.usta.api.resourceserver.hybris.converter.DuplicateCheckConverter;
import com.usta.api.resourceserver.hybris.model.DupCheckData;
import com.usta.api.resourceserver.hybris.model.DupCheckRequest;
import com.usta.api.resourceserver.hybris.model.HybrisTokenInfo;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import com.usta.api.resourceserver.hybris.service.HybrisService;
import com.usta.api.resourceserver.janrain.exception.JanRainTokenValidationException;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;
import com.usta.api.resourceserver.janrain.service.JanRainService;
import com.usta.api.resourceserver.login.model.LoginData;
import com.usta.api.resourceserver.login.model.LoginResult;
import com.usta.api.resourceserver.mail.exception.CheetachException;
import com.usta.api.resourceserver.mail.parameters.MailEntity;
import com.usta.api.resourceserver.mail.service.CheetachMailService;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Base64;

import static com.usta.api.resourceserver.mail.parameters.MailEntity.of;

@Service
public class LoginServiceImpl implements LoginService {
    private HybrisService hybrisService;

    private JanRainService janRainService;

    private CheetachMailService cheetachMailService;
    private DuplicateCheckConverter duplicateCheckConverter;

    LoginServiceImpl(HybrisService hybrisService,
                     JanRainService janRainService,
                     CheetachMailService cheetachMailService,
                     DuplicateCheckConverter duplicateCheckConverter) {
        this.hybrisService = hybrisService;
        this.janRainService = janRainService;
        this.cheetachMailService = cheetachMailService;
        this.duplicateCheckConverter = duplicateCheckConverter;
    }
    
    @Override
    public LoginResult login(LoginData loginData) throws JanRainTokenValidationException, CheetachException {
        JanRainUserData janRainUserData = janRainService.validateToken(loginData.getUuid(), loginData.getAccessToken());

        String givenName = janRainUserData.getGivenName();
        String familyName = janRainUserData.getFamilyName();
        String email = janRainUserData.getEmail();
    
        UserUpdateWsDTO hybrisInsertResult = hybrisService.insertCustomerDetails(loginData, janRainUserData);

        if (hybrisInsertResult.getCreationtime() != null) {
            sendWelcomeEmail(givenName, familyName, email);
        }

        HybrisTokenInfo hybrisTokenInfo = hybrisService.getHybrisToken(janRainUserData.getEmail());
        return prepareLoginResult(givenName, familyName, email, hybrisTokenInfo, hybrisInsertResult);
    }

    @Override
    public void duplicateCustomerValidation(@Validated DupCheckData dupCheckData) throws MuleLikeException {
        if(dupCheckData == null){
            throw new MuleLikeException(1096,"Request Missing");
        }

        if (dupCheckData.getGender() == null || "".equals(dupCheckData.getGender())) {
            dupCheckData.setGender("NA");
        }

        DupCheckRequest dupCheckRequest = duplicateCheckConverter.convert(dupCheckData);

        hybrisService.duplicateCustomerValidation(dupCheckRequest);
    }

    private void sendWelcomeEmail(String givenName, String familyName, String email) throws CheetachException {
        MailEntity mailEntity = of(email, givenName, familyName);
        cheetachMailService.triggerMail(mailEntity);
    }

    private LoginResult prepareLoginResult(String givenName, String familyName, String email, HybrisTokenInfo hybrisTokenInfo, UserUpdateWsDTO hybrisInsertResult) {
        LoginResult loginResult = new LoginResult();
        loginResult.setFirstNameCookie((null != givenName) ? new String(Base64.getEncoder().encode(givenName.getBytes())) : "");
        loginResult.setLastNameCookie((null != familyName) ? new String(Base64.getEncoder().encode(familyName.getBytes())) : "");
        loginResult.setEmailCookie((null != email) ? new String(Base64.getEncoder().encode(email.getBytes())) : "");
        loginResult.setHybrisAccessToken(hybrisTokenInfo.getAccessToken());
        loginResult.setLoginRefreshToken(hybrisTokenInfo.getRefreshToken());
        loginResult.setHybrisInsertResult(hybrisInsertResult);
        return loginResult;
    }

    /* MAGIC
        INPUT processing:
			JSONObject payload = new JSONObject(message.getPayload().toString());
			if(payload.has("dateOfBirth") && !("").equalsIgnoreCase(payload.get("dateOfBirth").toString())){
				String dateOfBirth = payload.get("dateOfBirth").toString();
				if(dateOfBirth.contains("T")){
					dateOfBirth = dateOfBirth.split("T")[0];
					dateOfBirth = dateOfBirth+"T00:00:00-0500";
				}
				payload.remove("dateOfBirth");
				payload.put("dateOfBirth", dateOfBirth);
				message.setPayload(payload);
			}

			if(!payload.has("uuid") || !payload.has("access_token")){
				message.setProperty(ASMFLOW, "true", PropertyScope.SESSION);
			}else{
				message.setProperty(ASMFLOW, "false", PropertyScope.SESSION);
			}

		LOGIN validation:
				logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " # Start ==> TraditionalLoginTransformer");
		InsertRequest response = new InsertRequest();
		ProviderDetails userPersonalDetailsBean = new ProviderDetails();
		String payload = null;
		String familyName = null;
		String givenName = null;
		String email = null;
		String userFlow = message.getInvocationProperty("userFlow");
		try {
			payload = message.getPayloadAsString();
		} catch (Exception e) {
			logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e);
			logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e.getMessage());
		}
		JSONObject payloadJson = new JSONObject(payload);
		if(null != payloadJson && payloadJson.has(ERRORS)){
			if(null != payloadJson.getJSONArray(ERRORS).getJSONObject(0) && payloadJson.getJSONArray(ERRORS).getJSONObject(0).has("type")){
				String type = payloadJson.getJSONArray(ERRORS).getJSONObject(0).get("type").toString();
				String errorMessage = payloadJson.getJSONArray(ERRORS).getJSONObject(0).get("message").toString();
				if(null != type && type.equalsIgnoreCase("UstaDuplicateCustomerError")){
					message.setProperty(EXCEPTIONMESSAGE, errorMessage, PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 1089, PropertyScope.SESSION);
					message.setProperty(CODE, 1089, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage(errorMessage));

				}

				if(null != type && type.equalsIgnoreCase("UstaDeceasedCustomerError")){
					message.setProperty(EXCEPTIONMESSAGE, errorMessage, PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 1122, PropertyScope.SESSION);
					message.setProperty(CODE, 1122, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage(errorMessage));

				}
			}
		}

		if(null != userFlow && ("user").equalsIgnoreCase(userFlow)){
			checkUserAge(payloadJson, message);
		}
		else{
			if(payloadJson.has("stat")){

				if(payloadJson.has("error_description")){
					String error = payloadJson.get("error_description").toString();
					message.setProperty(EXCEPTIONMESSAGE, "Invalid JanRain Token  or Token expired", PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 403, PropertyScope.SESSION);
					message.setProperty(CODE, 1004, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage("Invalid JanRain Token  or Token expired"));
				}
			}
			if(null != message.getProperty("mergeFlow", PropertyScope.SESSION) && message.getProperty("mergeFlow", PropertyScope.SESSION).toString().equalsIgnoreCase("true")){
				payloadJson = new JSONObject(message.getPayload().toString());
				if(payloadJson.has(RESULT)){
					JSONObject result = payloadJson.getJSONObject(RESULT);
					familyName = (result.get(FAMILY_NAME) != null) ? result.get(FAMILY_NAME).toString() : "";
					givenName = (result.get(GIVEN_NAME) != null) ? result.get(GIVEN_NAME).toString() : "";
					email = (result.get(EMAIL) != null) ? result.get(EMAIL).toString() : "";

				}
			}else{
				JSONObject result = new JSONObject(payloadJson.toString()).getJSONObject(RESULT);
				familyName = (result.get(FAMILY_NAME) != null) ? result.get(FAMILY_NAME).toString() : "";
				givenName = (result.get(GIVEN_NAME) != null) ? result.get(GIVEN_NAME).toString() : "";
				email = (result.get(EMAIL) != null) ? result.get(EMAIL).toString() : "";
			}

			String displayName = givenName;
			userPersonalDetailsBean.setDisplayName(displayName);
			JanRainName name = new JanRainName();
			name.setFamilyName(familyName);
			name.setGivenName(givenName);
			userPersonalDetailsBean.setName(name);
			userPersonalDetailsBean.setVerifiedEmail(email);
			PersonalAddress personalAddress = new PersonalAddress();
			// NET - 8145 REGISTRATION OBJECT CHANGE FOR NETGEN
			if(null != message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION).toString()))
				userPersonalDetailsBean.setDateOfBirth(message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION).toString());

			if(null != message.getProperty(POSTAL_CODE, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(POSTAL_CODE, PropertyScope.SESSION).toString())){
				personalAddress.setPostalcode(message.getProperty(POSTAL_CODE, PropertyScope.SESSION).toString());
				userPersonalDetailsBean.setPersonalAddress(personalAddress);
			}
			if(null != message.getProperty(GENDER, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(GENDER, PropertyScope.SESSION).toString()))
				userPersonalDetailsBean.setGender(message.getProperty(GENDER, PropertyScope.SESSION).toString());

			if(null != message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION).toString()))
				response.setSourceSystem(message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION).toString());


			response.setProviderDetails(userPersonalDetailsBean);
			message.setProperty("insertCustomerBody", new JSONObject(response).toString(), PropertyScope.SESSION);
			message.setProperty("firstNameCookie", ((null != givenName) ? new String(Base64.getEncoder().encode(givenName.getBytes())) : "") , PropertyScope.SESSION);
			message.setProperty("lastNameCookie", ((null != familyName) ? new String(Base64.getEncoder().encode(familyName.getBytes())) : ""), PropertyScope.SESSION);
			message.setProperty("emailCookie", ((null != email) ? new String(Base64.getEncoder().encode(email.getBytes())) : ""), PropertyScope.SESSION);

		}
		logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " # End ==> TraditionalLoginTransformer");
		return message;
     */
}
