package com.usta.api.resourceserver.hybris.model.dto;


import java.util.Date;

public class FacilityContactRoleMapWsDTO implements java.io.Serializable {


    private USTARoleWsDTO role;


    private Date startDate;


    private Date endDate;


    private String status;


    private String customerName;


    private String customerPhone;


    private String customerEmail;
}
