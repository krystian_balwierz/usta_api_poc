package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.List;

@Data
public class OpeningScheduleDataWsDTO implements java.io.Serializable {


    private String name;


    private String code;


    private List<WeekdayOpeningDayDataWsDTO> weekDayOpeningList;


    private List<SpecialOpeningDayData> specialDayOpeningList;
}
