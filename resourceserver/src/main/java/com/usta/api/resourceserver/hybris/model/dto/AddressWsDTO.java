package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class AddressWsDTO implements java.io.Serializable {
    private String id;
    private String title;
    private String titleCode;
    private String firstName;
    private String lastName;
    private String companyName;
    private String line1;
    private String line2;
    private String town;
    private RegionWsDTO region;
    private String postalCode;
    private String phone;
    private String email;
    private CountryWsDTO country;
    private Boolean shippingAddress;
    private Boolean visibleInAddressBook;
    private String formattedAddress;
    private String county;
    private String company;
    private boolean isPrimaryAddress;
    private String gender;
    private String phone1;
    private String line3;
    private String taLine3;
    private String locationDescription;
    private String taAddressType;
    private Double latitude;
    private Double longitude;
    private Double distance;
    private Boolean isValid;
    private String orgAddressStatus;
    private String firstname;
    private String lastname;
    private Date dateOfBirth;
    private String postalcode;
}
