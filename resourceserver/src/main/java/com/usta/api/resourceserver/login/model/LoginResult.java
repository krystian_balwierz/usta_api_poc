package com.usta.api.resourceserver.login.model;

import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import lombok.Data;

@Data
public class LoginResult {
    String firstNameCookie;
    String lastNameCookie;
    String emailCookie;
    String hybrisAccessToken;
    String loginRefreshToken;
    UserUpdateWsDTO hybrisInsertResult;
}
