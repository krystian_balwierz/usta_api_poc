package com.usta.api.resourceserver.hybris.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.usta.api.resourceserver.hybris.model.dto.ProviderDetails;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
public class InsertRequest {
    private ProviderDetails providerDetails;
    private String sourceSystem;
}
