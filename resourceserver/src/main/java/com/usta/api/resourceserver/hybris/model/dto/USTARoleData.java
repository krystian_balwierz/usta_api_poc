package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class USTARoleData implements java.io.Serializable {


    private String roleName;


    private String roleDescription;


    private String roleCode;


    private String status;


    private USTARoleData parentRole;
}
