package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class CountryData implements java.io.Serializable {


    private String isocode;


    private String name;
}
