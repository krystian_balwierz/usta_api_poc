package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserUpdateWsDTO implements java.io.Serializable {
    private String uid;
    private String displayName;
    private String gender;
    private String name;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private String company;
    private String ustaMemberID;
    private String uaid;
    private List<AddressWsDTO> addresses;
    private String usptaNumber;
    private String usptaCheckStatus;
    private String ptrCheckStatus;
    private String ptrNumber;
    private Date creationtime;
    private String ncsiRegistrantID;
    private String ncsiCheckStatus;
    private Date ncsiCheckTimestamp;
    private Date joinDate;
    private Date expiryDate;
    private String membershipStatus;
    private String parentEmail;
    private String taDistrict;
    private String taRegion;
    private Boolean wheelchairPlayer;
    private Boolean wheelChairPlayer;
    private Boolean wheelChairOption;
    private String membershipStatusEnum;
    private String profilePicture;
    private String contentType;
    private String fileName;
}
