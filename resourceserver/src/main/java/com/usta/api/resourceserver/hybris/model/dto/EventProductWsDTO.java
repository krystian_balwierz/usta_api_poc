package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EventProductWsDTO implements java.io.Serializable {


    private Date registrationStartDate;


    private Date registrationEndDate;


    private Boolean isUSTASanctioned;


    private Boolean hasEligibilityCriteria;


    private Boolean isPrivate;


    private Boolean isWheelChairEvent;


    private Date eventStartTime;


    private Date eventEndTime;


    private Date eventStartDate;


    private String name;


    private String code;


    private String spotLeft;


    private String eventCategoryName;


    private String description;


    private String registrationURL;


    private String registrationFeeDescription;


    private Integer registrants;


    private String sourceKey;


    private String sourceName;


    private String linkToPDF;


    private List<String> facilities;


    private String ageGroupDetail;


    private List<String> origin;


    private List<EligibilityCriteriaWsDTO> eligibilityCr;


    private List<EventContactRoleMapWsDTO> eventContactRole;


    private List<EventCustomerRoleMapWsDTO> eventCustomerRole;


    private List<FacilityAddressWsDTO> addresses;


    private CourtSizeWsDTO courtSize;


    private CourtSurfaceWsDTO courtSurface;


    private USTADistrictWsDTO district;


    private EventCategoryWsDTO eventCategory;


    private List<EventProductWsDTO> moreEvent;
}
