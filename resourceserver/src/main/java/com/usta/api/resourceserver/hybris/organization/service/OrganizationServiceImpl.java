package com.usta.api.resourceserver.hybris.organization.service;

import com.usta.api.resourceserver.hybris.connector.HybrisConnector;
import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganizationServiceImpl implements OrganizationService {
    @Autowired
    private HybrisConnector hybrisConnector;

    @Override
    public GetOrganizationResponse getOrganizationByEmail(String hybrisAccessToken, String emailId) {
/*                                              <set-session-variable variableName="orgEmail" value="#['ORGEMAIL']" doc:name="Session Variable"></set-session-variable>
                                                <custom-transformer class="com.usta.integration.validators.CommerceRequestValidator" doc:name="Java"></custom-transformer>
                                                <logger message="#[sessionVars.tokenForLogging + sessionVars.organizationId]" level="INFO" doc:name="Logger"></logger>
                                                <flow-ref name="Get_Organization" doc:name="Get_Organization"></flow-ref>*/
/*
        <logger level="INFO" message="payload to hybris #[payload:java.lang.String] and message is #[message]" doc:name="Logger"/>
            <https:outbound-endpoint exchange-pattern="request-response" host="${hybris_host}" port="${hybris_port}" path="${getOrgURL}" method="POST" connector-ref="HTTP_HTTPS" encoding="US-ASCII" contentType="application/json" doc:name="Get Organization"/>
        <object-to-string-transformer doc:name="Object to String"/>
        <logger message="#['Payload : '+payload]" level="INFO" doc:name="Logger"/>
         <choice doc:name="Choice">
            <when expression="#[payload != &quot;{\n}&quot;]">
                <logger message="#['1']" level="INFO" doc:name="Logger"/>
                <custom-transformer class="com.usta.integration.validators.CommerceResponseValidator" doc:name="Java"/>
            </when>
            <otherwise>
                <set-payload value="#[['Message': 'Record not found', 'ErrorCode':'400']]" doc:name="Set Payload"/>
                <json:object-to-json-transformer doc:name="Object to JSON"/>
            </otherwise>
        </choice>
 */
        return hybrisConnector.getOrganization(hybrisAccessToken, emailId, null);
    }

    @Override
    public GetOrganizationResponse getOrganizationByUid(String hybrisAccessToken, String organizationId) {
        /*                                  <custom-transformer class="com.usta.integration.validators.CommerceRequestValidator" doc:name="Java"></custom-transformer>
                                            <logger message="#[sessionVars.tokenForLogging + sessionVars.organizationId]" level="INFO" doc:name="Logger"></logger>
                                            <flow-ref name="Get_Organization_From_Uid" doc:name="Get_Organization_From_Uid"></flow-ref>*/

        /*
                <https:outbound-endpoint exchange-pattern="request-response" host="${hybris_host}" port="${hybris_port}" path="${getOrgFromUid}" method="POST" connector-ref="HTTP_HTTPS" encoding="US-ASCII" doc:name="Get Organization" contentType="application/json" responseTimeout="100000000">
        </https:outbound-endpoint>
        <object-to-string-transformer doc:name="Object to String"/>
        <logger message="#['Payload : '+payload]" level="INFO" doc:name="Logger"/>
         <choice doc:name="Choice">
            <when expression="#[payload != &quot;{\n}&quot;]">
                <logger message="#['1']" level="INFO" doc:name="Logger"/>
                <custom-transformer class="com.usta.integration.validators.CommerceResponseValidator" doc:name="Java"/>
            </when>
            <otherwise>
                <set-payload value="#[['Message': 'Record not found', 'ErrorCode':'400']]" doc:name="Set Payload"/>
                <json:object-to-json-transformer doc:name="Object to JSON"/>
            </otherwise>
        </choice>
         */
        return hybrisConnector.getOrganization(hybrisAccessToken, null, organizationId);
    }
}
