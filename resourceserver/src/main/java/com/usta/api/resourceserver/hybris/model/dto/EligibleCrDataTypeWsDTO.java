package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class EligibleCrDataTypeWsDTO implements java.io.Serializable {


    private String code;


    private String name;
}
