package com.usta.api.resourceserver.janrain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class JanRainUserData {
    private String id;
    private String uuid;
    private String email;
    private String familyName;
    private String givenName;

    private Date birthday;

    private String isChildAccount;
    private boolean underageCheckbox;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss.SSSSSS Z")
    private Date created;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss.SSSSSS Z")
    private Date lastUpdated;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss Z")
    private Date lastLogin;

}
