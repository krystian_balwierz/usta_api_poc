package com.usta.api.resourceserver.error.controller;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
class ErrorController {


    @GetMapping("/error")
    String error(@RequestBody String body) {

        log.info(body);
        return "logged";
    }

}
