package com.usta.api.resourceserver.janrain.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;
import lombok.Data;

@Data
@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
public class JanRainValidationStatus {

    private String stat;

    private String error;

    @JsonProperty("error_description")
    private String errorDescription;

    @JsonProperty("result")
    private JanRainUserData janRainUserData;

    public boolean isNotOk() {
        return !isOK();
    }

    public boolean isOK() {
        return "ok".equals(stat);
    }

}
