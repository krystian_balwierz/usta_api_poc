package com.usta.api.resourceserver.hybris.organization.controller;

import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;
import com.usta.api.resourceserver.hybris.organization.service.OrganizationService;
import com.usta.api.resourceserver.hybris.service.HybrisService;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Log
@RestController
class OrganizationController {

    OrganizationService organizationService;

    OrganizationController(final OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @PreAuthorize("#oauth2.hasAnyScope('READ','WRITE','UPDATE','CUSTOMER_INSERT')")
    @PostMapping("/getOrganization")
    public ResponseEntity<GetOrganizationResponse> getOrganization(@RequestParam(name = "hybris_access_token") String hybrisAccessToken, @RequestParam(required = false) String emailId, @RequestParam(required = false) String organizationId) throws Exception {
        if(emailId!=null){
            return ResponseEntity.status(HttpStatus.OK).body(organizationService.getOrganizationByEmail(hybrisAccessToken, emailId));
        } else{
            return ResponseEntity.status(HttpStatus.OK).body(organizationService.getOrganizationByUid(hybrisAccessToken, organizationId));
        }
    }
}
