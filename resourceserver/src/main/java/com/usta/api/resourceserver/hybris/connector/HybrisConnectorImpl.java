package com.usta.api.resourceserver.hybris.connector;

import com.usta.api.resourceserver.hybris.model.DupCheckRequest;
import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;
import com.usta.api.resourceserver.hybris.model.HybrisTokenResponse;
import com.usta.api.resourceserver.hybris.model.InsertRequest;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class HybrisConnectorImpl implements HybrisConnector {

    @Autowired
    @Qualifier("hybrisRestTemplate")
    RestTemplate hybrisRestTemplate;

    @Value("${hybris.insertCustomerDetails}")
    String insertCustomerDetailsUrl;
    @Value("${hybris.getOrgUrl}")
    String getOrgUrl;
    @Value("${hybris.auth-token.url}")
    String getTokenUrl;
    @Value("${hybris.customerDupeCheck}")
    String customerDupeCheckUrl;
    @Value("${hybris.url}")
    private String hybrisUrl;

    @Override
    public UserUpdateWsDTO insertCustomerDetails(InsertRequest insertRequest) {
        String url = hybrisUrl + insertCustomerDetailsUrl;
        ResponseEntity<UserUpdateWsDTO> entity = hybrisRestTemplate.postForEntity(url, insertRequest, UserUpdateWsDTO.class);
        return entity.getBody();
    }

    @Override
    public HybrisTokenResponse getHybrisToken(String email) {
        String url = hybrisUrl + getTokenUrl;
        ResponseEntity<HybrisTokenResponse> entity = hybrisRestTemplate.postForEntity(url, null, HybrisTokenResponse.class, email);
        return entity.getBody();
    }

    @Override
    public GetOrganizationResponse getOrganization(String hybrisAccessToken, String emailId, String organizationId) {
        String url = hybrisUrl + getOrgUrl;
        MultiValueMap<String, String> uriVariables = new LinkedMultiValueMap<>();
        uriVariables.add("access_token", hybrisAccessToken);
        uriVariables.add("code", emailId);
        uriVariables.add("uid", organizationId);
        URI uri = UriComponentsBuilder.fromUriString(url).queryParams(uriVariables).build().toUri();
        ResponseEntity<GetOrganizationResponse> entity = hybrisRestTemplate.postForEntity(uri, null, GetOrganizationResponse.class);
        return entity.getBody();
    }

    @Override
    public String customerDupeCheck(DupCheckRequest dupCheckRequest) {
        String url = hybrisUrl + customerDupeCheckUrl;
        ResponseEntity<String> entity = hybrisRestTemplate.postForEntity(url, dupCheckRequest, String.class);
        return entity.getBody();
    }
}
