package com.usta.api.resourceserver.janrain.service;

import com.usta.api.resourceserver.janrain.exception.JanRainTokenValidationException;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;
import com.usta.api.resourceserver.janrain.response.JanRainValidationStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class JanRainServiceImpl implements JanRainService {
    
    @Value("${usta.entity.url}")
    private String ustaEntityUrl;

    @Autowired
    @Qualifier("janRainRestTemplate")
    private RestTemplate restTemplate;
    
    @Override
    public JanRainUserData validateToken(String uuid, String accessToken) throws JanRainTokenValidationException {
        JanRainValidationStatus status = restTemplate.getForObject(ustaEntityUrl, JanRainValidationStatus.class, accessToken, uuid);
        
        if (status.isNotOk()) {
            throw new JanRainTokenValidationException(status.getError()+" : "+status.getErrorDescription());
        }
        return status.getJanRainUserData();
    }
    
}
