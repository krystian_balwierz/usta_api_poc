package com.usta.api.resourceserver.hybris.section.model;

import lombok.Data;
import lombok.Setter;

import java.util.List;

import static lombok.AccessLevel.NONE;

@Data
public class SectionResponse {
    
    @Setter(NONE)
    private final List<String> sectionList;
    
}