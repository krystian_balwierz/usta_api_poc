package com.usta.api.resourceserver.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

//TODO add exception handling to Spring - Mule-like message should be returned
@Getter
@AllArgsConstructor
public class MuleLikeException extends Throwable {
    private int code;
    private String message;
}
