package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class SocialAddressWsDTO implements java.io.Serializable {


    private String socialMediaID;


    private String socialMediaType;


    private String socialMediaPurpose;


    private String status;
}
