package com.usta.api.resourceserver.janrain.exception;

public class JanRainTokenValidationException extends Exception {
    public JanRainTokenValidationException(String error) {
        super(error);
    }
}
