package com.usta.api.resourceserver.login.controller;

import com.usta.api.resourceserver.exception.MuleLikeException;
import com.usta.api.resourceserver.hybris.model.DupCheckData;
import com.usta.api.resourceserver.janrain.exception.JanRainTokenValidationException;
import com.usta.api.resourceserver.login.model.LoginData;
import com.usta.api.resourceserver.login.model.LoginResponse;
import com.usta.api.resourceserver.login.model.LoginResult;
import com.usta.api.resourceserver.login.service.LoginService;
import com.usta.api.resourceserver.mail.exception.CheetachException;
import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;


@Log
@RestController
class LoginController {

    LoginService loginService;

    LoginController(final LoginService loginService) {
        this.loginService = loginService;
    }

    @PreAuthorize("#oauth2.hasScope('CUSTOMER_INSERT')")
    @PostMapping("/traditionalLogin")
    public ResponseEntity<LoginResponse> traditionalLogin(@RequestBody LoginData loginData, HttpServletResponse response)
            throws JanRainTokenValidationException, CheetachException {
        LoginResult loginResult = loginService.login(loginData);
        LoginResponse loginResponse = prepareResponse(loginResult);
        addCookies(response, loginResult);
        return ResponseEntity.status(HttpStatus.OK).body(loginResponse);
    }

    @PreAuthorize("#oauth2.hasScope('CUSTOMER_INSERT')")
    @PostMapping("/duplicateCustomerValidation")
    public ResponseEntity<String> duplicateCustomerValidation(@RequestBody DupCheckData dupCheckData, @RequestParam String type) throws MuleLikeException {
        if ("lightweightaccount".equals(type)) {
            loginService.duplicateCustomerValidation(dupCheckData);
            return ResponseEntity.status(HttpStatus.OK).body("{\n\t\"message\": \"Duplicate Customer Validation Successful\" ,\n" +
                    "\t\"type\": \"success\",\n" +
                    "\t\"code\" : 200\n}");
        } else {
            return ResponseEntity.status(HttpStatus.OK).body("{\n\t\"message\": \"Duplicate Customer Validation Successful\" ,\n" +
                    "\t\"type\": \"success\",\n" +
                    "\t\"code\" : 200\n}");
        }
    }

    private void addCookies(HttpServletResponse response, LoginResult loginResult) {
        response.addCookie(getCookie("emailId", loginResult.getEmailCookie()));
        response.addCookie(getCookie("firstName", loginResult.getFirstNameCookie()));
        response.addCookie(getCookie("lastName", loginResult.getLastNameCookie()));
        Cookie oAuthCookie = getCookie("OAuthToken", loginResult.getHybrisAccessToken());
        oAuthCookie.setSecure(false);
        response.addCookie(oAuthCookie);
    }

    private Cookie getCookie(String cookieName, String cookieValue) {
        log.log(Level.INFO, "Saving cookie {0} {1}", new Object[]{cookieName, cookieValue});
        Cookie cookie = new Cookie(cookieName, cookieValue);
        cookie.setDomain("usta.com");
        cookie.setPath("/");
        cookie.setHttpOnly(false);
        cookie.setSecure(true);
        return cookie;
    }

    private LoginResponse prepareResponse(LoginResult loginResult) {
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setUserLoginToken(loginResult.getHybrisAccessToken());
        loginResponse.setHybrisInsertResult(loginResult.getHybrisInsertResult());
        loginResponse.setHybrisRefreshToken(loginResult.getLoginRefreshToken());
        return loginResponse;
    }
}
