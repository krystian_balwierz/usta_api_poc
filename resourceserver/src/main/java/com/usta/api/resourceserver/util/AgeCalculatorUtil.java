package com.usta.api.resourceserver.util;

import com.usta.api.resourceserver.exception.MuleLikeException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AgeCalculatorUtil {

    public static int age(String dateOfBirth) throws MuleLikeException {
        Date customerDob;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            customerDob = df.parse(dateOfBirth);

        } catch (ParseException e) {
            throw new MuleLikeException(1098, "DOB is not in correct format ");
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(customerDob);

        LocalDate today = LocalDate.now();
        LocalDate birthday = LocalDate.of(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH) + 1), cal.get(Calendar.DATE));
        Period period = Period.between(birthday, today);
        return period.getYears();
    }


}
