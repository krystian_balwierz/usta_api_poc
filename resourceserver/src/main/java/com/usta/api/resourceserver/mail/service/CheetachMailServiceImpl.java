package com.usta.api.resourceserver.mail.service;

import com.usta.api.resourceserver.mail.exception.CheetachException;
import com.usta.api.resourceserver.mail.parameters.MailEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class CheetachMailServiceImpl implements CheetachMailService {

    @Autowired
    @Qualifier("cheetahRestTemplate")
    private RestTemplate restTemplate;
    
    @Value("${cheetach.login.url}")
    private String CHEETACH_LOGIN_URL;
    
    @Value("${cheetach.trigger.url}")
    private String CHEETACH_MAIL_TRIGGER_URL;

    @Override
    public void loginMail() throws CheetachException {
        
        String response = restTemplate.getForObject(CHEETACH_LOGIN_URL, String.class);
        
        System.out.println(response);
        
        checkResponseIsOk(response, "Login to cheetach failed");
    }
    
    @Override
    public void triggerMail(MailEntity mailEntity) throws CheetachException {
        loginMail();

        String email = mailEntity.getEmail();
        String fname = mailEntity.getFname();
        String lname = mailEntity.getLname();
        
        String response = restTemplate.getForObject(CHEETACH_MAIL_TRIGGER_URL, String.class, email, fname, lname);
        
        System.out.println(response);
        
        checkResponseIsOk(response, "Mail trigger failed");
    }
    
}
