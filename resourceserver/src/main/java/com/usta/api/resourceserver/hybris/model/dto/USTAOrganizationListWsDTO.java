package com.usta.api.resourceserver.hybris.model.dto;

import java.util.List;

public  class USTAOrganizationListWsDTO  implements java.io.Serializable
{
    private List<USTAOrganizationWsDTO> ustaOrganizations;
}
