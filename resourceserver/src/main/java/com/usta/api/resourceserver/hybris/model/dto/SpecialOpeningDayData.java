package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class SpecialOpeningDayData extends OpeningDayData {


    private Date date;


    private String formattedDate;


    private boolean closed;


    private String name;


    private String comment;
}
