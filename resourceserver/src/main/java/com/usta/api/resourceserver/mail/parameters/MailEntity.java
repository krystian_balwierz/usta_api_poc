package com.usta.api.resourceserver.mail.parameters;

import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

import static lombok.AccessLevel.NONE;

@ToString
@Getter
public class MailEntity {
    
    private String email;
    private String fname;
    private String lname;
    
    @Getter(NONE)
    private Map<String, String> mailParameters;
    
    public MailEntity(String email, String fname, String lname, Map<String, String> mailParameters) {
        this.email = email;
        this.fname = fname;
        this.lname = lname;
        this.mailParameters = mailParameters;
    }
    
    public static MailEntity of(String email, String fname, String lname) {
        return new MailEntity(email, fname, lname, new HashMap<>());
    }
 
    public String getParameter(String paramKey) {
        return mailParameters.get(paramKey);
    }
    
    public void putParameter(String key, String value) {
        mailParameters.put(key, value);
    }
    
}