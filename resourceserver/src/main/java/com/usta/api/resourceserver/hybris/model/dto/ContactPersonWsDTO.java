package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ContactPersonWsDTO implements java.io.Serializable {


    private String status;


    private String name;


    private String pocType;


    private Set<EventContactRoleMapWsDTO> contactEventRole;


    private Set<ProgramContactRoleMapWsDTO> contactProgramRole;


    private FacilityContactRoleMapWsDTO contactFacilityRole;


    private USTAOrganizationWsDTO ustaOrganization;


    private Set<CategoryContactRoleMapWsDTO> contactEventCatRole;


    private Set<USTAOrganizationRoleMapWsDTO> ustaOrganizationRoleMap;


    private List<AddressData> addresses;
}
