package com.usta.api.resourceserver.login.service;

import com.usta.api.resourceserver.exception.MuleLikeException;
import com.usta.api.resourceserver.hybris.model.DupCheckData;
import com.usta.api.resourceserver.janrain.exception.JanRainTokenValidationException;
import com.usta.api.resourceserver.login.model.LoginData;
import com.usta.api.resourceserver.login.model.LoginResult;
import com.usta.api.resourceserver.mail.exception.CheetachException;

public interface LoginService {
    LoginResult login(LoginData loginData) throws JanRainTokenValidationException, CheetachException;

    void duplicateCustomerValidation(DupCheckData dupCheckData) throws MuleLikeException;
}
