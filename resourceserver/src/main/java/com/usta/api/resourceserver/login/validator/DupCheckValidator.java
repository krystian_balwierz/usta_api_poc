package com.usta.api.resourceserver.login.validator;

import com.usta.api.resourceserver.exception.MuleLikeException;
import com.usta.api.resourceserver.hybris.model.DupCheckData;
import com.usta.api.resourceserver.util.AgeCalculatorUtil;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
@Log
public class DupCheckValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return DupCheckData.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DupCheckData dupCheckData = (DupCheckData) target;
        /* bellow code is taken from com.usta.integration.consumerregistration.DuplicateCheckReqValidator */


        if (dupCheckData.getUaid() == null || "".equals(dupCheckData.getUaid())) {
            errors.reject("1098", "Mandatory Parameter Missing : uaid ");
        }

        if (dupCheckData.getFirstName() == null || "".equals(dupCheckData.getFirstName())) {
            errors.reject("1098", "Mandatory Parameter Missing : firstName ");
        }

        if (dupCheckData.getLastName() == null || "".equals(dupCheckData.getLastName())) {
            errors.reject("1098","Mandatory Parameter Missing : lastName ");
        }

        if (dupCheckData.getDateOfBirth() == null || "".equals(dupCheckData.getDateOfBirth())) {
            errors.reject("1098", "Mandatory Parameter Missing : dateOfBirth ");
        } else {
            String dateOfBirth = dupCheckData.getDateOfBirth();
            int age = 0;
            try {
                age = AgeCalculatorUtil.age(dateOfBirth);
            } catch (MuleLikeException e) {
                errors.reject("1098", "DOB is not in correct format");
            }

            log.info(" User's Age : " + age);
            if (age < 13) {
                errors.reject("1101", "User is less than 13");
            }
        }
    }
}
