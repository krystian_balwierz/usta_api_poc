package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

import java.util.Date;

@Data
public class USTATermsWsDTO implements java.io.Serializable {


    private String acceptedTermsAndCondiVers;


    private Date acceptedDate;


    private Boolean accepted;
}
