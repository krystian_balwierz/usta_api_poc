package com.usta.api.resourceserver.hybris.service;

import com.usta.api.resourceserver.hybris.connector.HybrisConnector;
import com.usta.api.resourceserver.hybris.model.*;
import com.usta.api.resourceserver.hybris.model.dto.PersonalAddress;
import com.usta.api.resourceserver.hybris.model.dto.ProviderDetails;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;
import com.usta.api.resourceserver.login.model.LoginData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HybrisServiceImpl implements HybrisService {
    @Autowired
    private HybrisConnector hybrisConnector;

    @Override
    public HybrisTokenInfo getHybrisToken(String email) {
        HybrisTokenResponse response = hybrisConnector.getHybrisToken(email);

        return new HybrisTokenInfo(response);

        /* MAGIC
        		String isError = FALSE;
		if(null == message.getPayload() || ("").equalsIgnoreCase(message.getPayload().toString())){
			isError = TRUE;
		}

		JSONObject payload = new JSONObject(message.getPayload().toString());
		if(payload.has("errors")){
			JSONArray errors = payload.getJSONArray("errors");
			String innerErrors = (new JSONObject(errors.get(0).toString())).get("type").toString();
			String errorMessage = (new JSONObject(errors.get(0).toString())).has("message") ? (new JSONObject(errors.get(0).toString())).get("message").toString() : "";

			logger.error(message.getProperty("tokenForLogging", PropertyScope.SESSION) + " Error Type : " +innerErrors.replaceAll("\\n", " ") + " Error Message : " +errorMessage.replaceAll("\\n", " "));
			isError = TRUE;
		}else if(payload.has("error")){
			String error = (payload.get("error_description") != null) ? payload.get("error_description").toString() : null;
			if(null != error)
				logger.error(message.getProperty("tokenForLogging", PropertyScope.SESSION) + " Error Description : " +error.replaceAll("\\n", " "));
			isError = TRUE;
		}
		message.setProperty("ISERROR", isError, PropertyScope.SESSION);
		logger.info(message.getProperty("tokenForLogging", PropertyScope.SESSION) + " ISERROR Value : " +isError);
		return message;
         */

        //TODO: CommerceResponseValidator

        /*
            #[json:access_token] is set in hybrisAccessToken session variable
            #[json:refresh_token] is set in hybrisRefreshToken session variable
            #[json:expires_in] is set in hybris_expires_in session variable
        */

        //TODO: CustomerLoginHybrisSession
    }

    @Override
    public UserUpdateWsDTO insertCustomerDetails(LoginData loginData, JanRainUserData janRainUserData) {
        ProviderDetails userPersonalDetailsBean = new ProviderDetails();
        InsertRequest insertRequest = new InsertRequest();
        resolveDataFromJanRain(janRainUserData, userPersonalDetailsBean);
        resolveDataFromRequest(loginData, userPersonalDetailsBean, insertRequest);

        UserUpdateWsDTO userUpdateResponse = hybrisConnector.insertCustomerDetails(insertRequest);
        return userUpdateResponse;


/* MAGIC
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " # Start ==> TraditionalLoginTransformer");
		InsertRequest response = new InsertRequest();
		ProviderDetails userPersonalDetailsBean = new ProviderDetails();
		String payload = null;
		String familyName = null;
		String givenName = null;
		String email = null;
		String userFlow = message.getInvocationProperty("userFlow");
		try {
			payload = message.getPayloadAsString();
		} catch (Exception e) {
			logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e);
			logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e.getMessage());
		}
		JSONObject payloadJson = new JSONObject(payload);
		if(null != payloadJson && payloadJson.has(ERRORS)){
			if(null != payloadJson.getJSONArray(ERRORS).getJSONObject(0) && payloadJson.getJSONArray(ERRORS).getJSONObject(0).has("type")){
				String type = payloadJson.getJSONArray(ERRORS).getJSONObject(0).get("type").toString();
				String errorMessage = payloadJson.getJSONArray(ERRORS).getJSONObject(0).get("message").toString();
				if(null != type && type.equalsIgnoreCase("UstaDuplicateCustomerError")){
					message.setProperty(EXCEPTIONMESSAGE, errorMessage, PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 1089, PropertyScope.SESSION);
					message.setProperty(CODE, 1089, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage(errorMessage));

				}

				if(null != type && type.equalsIgnoreCase("UstaDeceasedCustomerError")){
					message.setProperty(EXCEPTIONMESSAGE, errorMessage, PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 1122, PropertyScope.SESSION);
					message.setProperty(CODE, 1122, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage(errorMessage));

				}
			}
		}

		if(null != userFlow && ("user").equalsIgnoreCase(userFlow)){
			checkUserAge(payloadJson, message);
		}
		else{
			if(payloadJson.has("stat")){

				if(payloadJson.has("error_description")){
					String error = payloadJson.get("error_description").toString();
					message.setProperty(EXCEPTIONMESSAGE, "Invalid JanRain Token  or Token expired", PropertyScope.SESSION);
					message.setProperty(ERRORCODE, 403, PropertyScope.SESSION);
					message.setProperty(CODE, 1004, PropertyScope.SESSION);
					throw new TransformerException(MessageFactory.createStaticMessage("Invalid JanRain Token  or Token expired"));
				}
			}
			if(null != message.getProperty("mergeFlow", PropertyScope.SESSION) && message.getProperty("mergeFlow", PropertyScope.SESSION).toString().equalsIgnoreCase("true")){
				payloadJson = new JSONObject(message.getPayload().toString());
				if(payloadJson.has(RESULT)){
					JSONObject result = payloadJson.getJSONObject(RESULT);
					familyName = (result.get(FAMILY_NAME) != null) ? result.get(FAMILY_NAME).toString() : "";
					givenName = (result.get(GIVEN_NAME) != null) ? result.get(GIVEN_NAME).toString() : "";
					email = (result.get(EMAIL) != null) ? result.get(EMAIL).toString() : "";

				}
			}else{
				JSONObject result = new JSONObject(payloadJson.toString()).getJSONObject(RESULT);
				familyName = (result.get(FAMILY_NAME) != null) ? result.get(FAMILY_NAME).toString() : "";
				givenName = (result.get(GIVEN_NAME) != null) ? result.get(GIVEN_NAME).toString() : "";
				email = (result.get(EMAIL) != null) ? result.get(EMAIL).toString() : "";
			}

			String displayName = givenName;
			userPersonalDetailsBean.setDisplayName(displayName);
			JanRainName name = new JanRainName();
			name.setFamilyName(familyName);
			name.setGivenName(givenName);
			userPersonalDetailsBean.setName(name);
			userPersonalDetailsBean.setVerifiedEmail(email);
			PersonalAddress personalAddress = new PersonalAddress();
			// NET - 8145 REGISTRATION OBJECT CHANGE FOR NETGEN
			if(null != message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION).toString()))
				userPersonalDetailsBean.setDateOfBirth(message.getProperty(DATE_OF_BIRTH, PropertyScope.SESSION).toString());

			if(null != message.getProperty(POSTAL_CODE, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(POSTAL_CODE, PropertyScope.SESSION).toString())){
				personalAddress.setPostalcode(message.getProperty(POSTAL_CODE, PropertyScope.SESSION).toString());
				userPersonalDetailsBean.setPersonalAddress(personalAddress);
			}
			if(null != message.getProperty(GENDER, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(GENDER, PropertyScope.SESSION).toString()))
				userPersonalDetailsBean.setGender(message.getProperty(GENDER, PropertyScope.SESSION).toString());

			if(null != message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION) && !("").equalsIgnoreCase(message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION).toString()))
				response.setSourceSystem(message.getProperty(SOURCE_SYSTEM, PropertyScope.SESSION).toString());


			response.setProviderDetails(userPersonalDetailsBean);
			message.setProperty("insertCustomerBody", new JSONObject(response).toString(), PropertyScope.SESSION);
			message.setProperty("firstNameCookie", ((null != givenName) ? new String(Base64.getEncoder().encode(givenName.getBytes())) : "") , PropertyScope.SESSION);
			message.setProperty("lastNameCookie", ((null != familyName) ? new String(Base64.getEncoder().encode(familyName.getBytes())) : ""), PropertyScope.SESSION);
			message.setProperty("emailCookie", ((null != email) ? new String(Base64.getEncoder().encode(email.getBytes())) : ""), PropertyScope.SESSION);

		}
		logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " # End ==> TraditionalLoginTransformer");
		return message;
	}

	public void checkUserAge(JSONObject payloadJson, MuleMessage message) throws TransformerException{
		if(payloadJson.has(DATE_OF_BIRTH))
		{
			if (payloadJson.get(DATE_OF_BIRTH).toString().length()> 4)
			{
				String customerCategory = message.getProperty("emailTemplate", PropertyScope.SESSION);

				if ( null!=customerCategory && (customerCategory.equalsIgnoreCase("usta-wcms") || customerCategory.equalsIgnoreCase("usta-netgen"))){
					String payloadDob=payloadJson.get(DATE_OF_BIRTH).toString();
					logger.info(message.getProperty("tokenForLogging", PropertyScope.SESSION) + " Customer dob : "+ payloadDob);
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
					Date customerDob;
					int age=0;
					try {
						customerDob = df.parse(payloadDob);
						Date today = new Date();
						int dobYear = customerDob.getYear();
						int dobMonth = customerDob.getMonth();
						int dobDay = customerDob.getDate();
						age = today.getYear() - dobYear;
						if (dobMonth > today.getMonth()) { // this year can't be counted!
							age--;
						} else if (dobMonth == today.getMonth()) { // same month? check for day
							if (dobDay > today.getDate()) { // this year can't be counted!
								age--;
							}
						}
					} catch (ParseException e) {
						logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e);
						logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " " +e.getMessage());
						logger.error(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " Error while parsing parsing customer DOB");
					}
					logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " Customer age : "+ age);
					if (age < 13)
					{
						logger.info(message.getProperty(TOKENFORLOGGING, PropertyScope.SESSION) + " Customer age is not eligible for login");
						message.setProperty(EXCEPTIONMESSAGE, "Customer age is less than 13", PropertyScope.SESSION);
						message.setProperty(ERRORCODE, 1041, PropertyScope.SESSION);
						message.setProperty(CODE, 1041, PropertyScope.SESSION);
						throw new TransformerException(MessageFactory.createStaticMessage("Customer age is less than 13"));
					}
				}
			}
		}
	}
 */
    }

    @Override
    public void duplicateCustomerValidation(DupCheckRequest dupCheckRequest) {
        hybrisConnector.customerDupeCheck(dupCheckRequest);
    }

    private void resolveDataFromRequest(LoginData loginData, ProviderDetails userPersonalDetailsBean, InsertRequest insertRequest) {
        // NET - 8145 REGISTRATION OBJECT CHANGE FOR NETGEN
        if (loginData.getDateOfBirth() != null) {
            userPersonalDetailsBean.setDateOfBirth(loginData.getDateOfBirth());
        }

        if (loginData.getPostalCode() != null) {
            PersonalAddress personalAddress = new PersonalAddress();
            personalAddress.setPostalcode(loginData.getPostalCode());
            userPersonalDetailsBean.setPersonalAddress(personalAddress);
        }
        if (loginData.getGender() != null)
            userPersonalDetailsBean.setGender(loginData.getGender());

        if (loginData.getSourceSystem() != null)
            insertRequest.setSourceSystem(loginData.getSourceSystem());

        insertRequest.setProviderDetails(userPersonalDetailsBean);
        insertRequest.setSourceSystem(loginData.getSourceSystem());
    }

    private void resolveDataFromJanRain(JanRainUserData janRainUserData, ProviderDetails userPersonalDetailsBean) {
        String givenName = janRainUserData.getGivenName();
        String displayName = givenName;
        String familyName = janRainUserData.getFamilyName();
        String email = janRainUserData.getEmail();

        userPersonalDetailsBean.setDisplayName(displayName);

        JanRainName name = new JanRainName();
        name.setFamilyName(familyName);
        name.setGivenName(givenName);

        userPersonalDetailsBean.setName(name);
        userPersonalDetailsBean.setVerifiedEmail(email);
    }
}
