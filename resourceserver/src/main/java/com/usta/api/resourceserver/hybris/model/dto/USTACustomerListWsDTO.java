package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

import java.util.List;

@Data
public  class USTACustomerListWsDTO  implements java.io.Serializable
{
    private List<UserUpdateWsDTO> ustaCustomer;
}
