package com.usta.api.resourceserver.hybris.model;

import lombok.Data;

@Data
public class DupCheckRequest {
    String uaid;
    String firstName;
    String lastName;
    String dateOfBirth;
    String lightWeightAccount;
}
