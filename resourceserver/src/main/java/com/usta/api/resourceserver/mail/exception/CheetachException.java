package com.usta.api.resourceserver.mail.exception;

public class CheetachException extends Exception {
    
    public CheetachException(final String message) {
        super(message);
    }
    
}