package com.usta.api.resourceserver.interceptor;

import lombok.extern.java.Log;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Level;

@Log
public class LogRestTemplateInterceptor implements ClientHttpRequestInterceptor {

    private final static Level LOG_LEVEL = Level.INFO;

    @Override
    public ClientHttpResponse intercept(
            HttpRequest request, byte[] body,
            ClientHttpRequestExecution execution) throws IOException {

        logRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);

        logResponse(response);
        return response;
    }

    private void logResponse(ClientHttpResponse response) throws IOException {
        log.log(LOG_LEVEL, "-----------------------------------------------------");
        log.log(LOG_LEVEL, "Response Headers: {0}", response.getHeaders());
        log.log(LOG_LEVEL, "Response Status: {0}, {1}", new Object[] {response.getStatusCode(), response.getStatusText()});
        log.log(LOG_LEVEL, "Response Body: {0}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()) );
        log.log(LOG_LEVEL, "-----------------------------------------------------");
    }

    private void logRequest(HttpRequest request, byte[] body) {
        log.log(LOG_LEVEL, "-----------------------------------------------------");
        log.log(LOG_LEVEL, "Request URI: {0}", request.getURI());
        log.log(LOG_LEVEL, "Request Headers: {0}", request.getHeaders());
        log.log(LOG_LEVEL, "Request Method: {0}", request.getMethod());
        log.log(LOG_LEVEL, "Request Body: {0}", new String(body, Charset.defaultCharset()));
        log.log(LOG_LEVEL, "-----------------------------------------------------");
    }
}