package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FacilityWsDTO implements java.io.Serializable {


    private String facilityStatus;


    private String imageLink;


    private Boolean isWheelChairAccessible;


    private Boolean isParkingAvailable;


    private Boolean isPlayerMatchingAvailable;


    private Boolean isReservationRequired;


    private Boolean hasHittingWall;


    private Boolean hasBallMachine;


    private Boolean hasProShop;


    private Boolean isActive;


    private Boolean hasPrograms;


    private Boolean hasSpanishSpeakingTranslator;


    private Boolean isLit;


    private Boolean has36;


    private Boolean has60;


    private Integer totalCourtsCount;


    private Date lastValidatedDate;


    private String hoursOfOpening;


    private String sourceKey;


    private String sourceName;


    private Boolean isPrivate;


    private Integer indoorCourtCount;


    private Integer outdoorCourtCount;


    private Boolean hasGrassCourt;


    private Boolean hasHardCourt;


    private Boolean hasClayCourt;


    private Boolean hasOtherCourt;


    private Boolean has78;


    private Boolean isCoachingAvailable;


    private Boolean isEquipmentAvailable;


    private Boolean offersClasses;


    private Boolean hasRacquetRental;


    private Boolean hasYouthEquipment;


    private String contactName;


    private Double latitude;


    private Double longitude;


    private String name;


    private String displayName;


    private String url;


    private String description;


    private String storeContent;


    private Boolean isNetgenFacility;


    private String facilityEmail;


    private AddressWsDTO address;


    private OpeningScheduleDataWsDTO openingHours;


    private List<USTAPhoneWsDTO> facilityPhone;


    private List<FacilityAddressWsDTO> additionalAddr;


    private List<SocialAddressWsDTO> socialAddr;


    private List<EmailAddressWsDTO> emailAddress;


    private List<FacilityContactRoleMapWsDTO> facilityContactRole;


    private List<FacilityCustomerRoleMapWsDTO> facilityCustomerRole;


    private FacilityType facilityType;


    private FacilitySubType facilitySubType;


    private List<EventProductWsDTO> eventProduct;


    private List<EventCategoryWsDTO> eventCategory;


    private String moreFacility;
}
