package com.usta.api.resourceserver.hybris.service;

import com.usta.api.resourceserver.hybris.model.DupCheckRequest;
import com.usta.api.resourceserver.hybris.organization.model.GetOrganizationResponse;
import com.usta.api.resourceserver.hybris.model.HybrisTokenInfo;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import com.usta.api.resourceserver.janrain.model.JanRainUserData;
import com.usta.api.resourceserver.login.model.LoginData;

public interface HybrisService {
    HybrisTokenInfo getHybrisToken(String email);

    UserUpdateWsDTO insertCustomerDetails(LoginData loginData, JanRainUserData janRainUserData);

    void duplicateCustomerValidation(DupCheckRequest dupCheckRequest);
}
