package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class USTAOrganizationWsDTO implements java.io.Serializable {


    private String uid;


    private String name;


    private String sourceName;


    private String sourceKey;


    private String uaid;


    private OrganizationTypeWsDTO orgType;


    private OrganizationSubTypeWsDTO orgSubType;


    private Boolean receivedProviderPack;


    private Date receivedDateOfProviderPack;


    private Boolean isNetGenOrg;


    private Date netGenUpdatedDate;


    private Boolean isTrainingReceived;


    private Date trainingCompletionDate;


    private Boolean hasCommunityPartner;


    private EmailAddressWsDTO principalEmailAddress;


    private String schoolDistrict;


    private String orgInitialSourcecode;


    private String orgNickName;


    private Date orgExpiryDate;


    private String orgStatus;


    private Date modifiedTime;


    private String modifiedTimeWithMilliSecs;


    private Date creationTime;


    private Date joinDate;


    private String membershipStatus;


    private List<USTAPhoneWsDTO> ustaPhone;


    private List<SocialAddressWsDTO> socialAddress;


    private List<FacilityWsDTO> facility;


    private List<EmailAddressWsDTO> emailAddress;


    private List<USTATermsWsDTO> ustaTerms;


    private List<USTAOrganizationRoleMapWsDTO> ustaOrganizationRoleMap;


    private List<ProviderOrganizationMapWsDTO> ustaCustOrgRole;


    private List<ContactPersonWsDTO> pointOfContact;


    private List<AddressData> addresses;


    private String communityPartnerApproved;


    private Date communityPartnerDateTime;
}
