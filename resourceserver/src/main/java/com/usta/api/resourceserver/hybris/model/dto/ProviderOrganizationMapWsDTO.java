package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class ProviderOrganizationMapWsDTO implements java.io.Serializable {


    private String status;


    private Date startDate;


    private Date endDate;


    private String outSideOrgName;


    private Boolean active;


    private USTAOrganizationProviderWsDTO ustaOrg;


    private USTARoleWsDTO orgProviderRole;


    private String orgOtherRole;


    private UserUpdateWsDTO ustaCustomer;


    private String netGenAdminStatus;


    private USTARoleWsDTO role;
}
