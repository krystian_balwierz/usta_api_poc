package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class EventCategoryWsDTO implements java.io.Serializable {


    private String season;


    private Integer year;


    private Boolean isUSTASanctioned;


    private String registrationFeeDescription;


    private String sourceName;


    private String sourceKey;


    private String USTAAreaCode;


    private String USTAAreaName;


    private String USTASectionCode;


    private String USTASectionName;


    private USTADistrictWsDTO district;


    private String ZipCode;


    private Integer Radius;


    private String ZipWithinRadius;


    private String name;


    private String superCategoryName;
}
