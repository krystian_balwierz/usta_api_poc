package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class FacilityType implements java.io.Serializable {


    private String name;


    private String code;
}
