package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class EmailAddressWsDTO implements java.io.Serializable {


    private Boolean isPrimaryEmailAddress;


    private Boolean isEmailVerifed;


    private String emailPurpose;


    private Boolean isPublic;


    private String emailAddress;


    private String emailAddr;

}
