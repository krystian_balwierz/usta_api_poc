package com.usta.api.resourceserver.login.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.usta.api.resourceserver.hybris.model.dto.UserUpdateWsDTO;
import lombok.Data;

@Data
public class LoginResponse {
    @JsonUnwrapped
    UserUpdateWsDTO hybrisInsertResult;

    @JsonProperty("hybris_refresh_token")
    String hybrisRefreshToken;

    @JsonProperty("user_login_token")
    String userLoginToken;
}
