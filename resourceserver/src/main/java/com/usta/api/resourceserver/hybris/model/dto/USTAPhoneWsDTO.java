package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class USTAPhoneWsDTO implements java.io.Serializable {


    private String phoneNumber;


    private String phoneType;


    private String phoneExtension;


    private String countryCode;


    private String areaCode;


    private String note;


    private String status;


    private Boolean isPrimaryPhone;


    private Boolean isPublic;

}
