package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

@Data
public class ProgramContactRoleMapWsDTO implements java.io.Serializable {


    private String firstName;


    private String lastName;


    private String email;


    private String phoneNumber;


    private boolean isProvider;


    private boolean isPrimaryContact;
}
