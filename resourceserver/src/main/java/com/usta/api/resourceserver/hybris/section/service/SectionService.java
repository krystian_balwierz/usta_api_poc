package com.usta.api.resourceserver.hybris.section.service;

import com.usta.api.resourceserver.hybris.section.model.SectionResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.MissingServletRequestParameterException;

public interface SectionService {
    
    SectionResponse doRequest(final String accessToken, MultiValueMap<String, String> parameters)
        throws MissingServletRequestParameterException;
    
}
