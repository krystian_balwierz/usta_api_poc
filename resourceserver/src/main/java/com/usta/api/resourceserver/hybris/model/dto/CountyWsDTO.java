package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class CountyWsDTO implements java.io.Serializable {


    private String code;


    private String name;


    private String description;


    private Boolean isActive;

}
