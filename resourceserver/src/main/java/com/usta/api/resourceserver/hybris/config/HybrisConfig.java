package com.usta.api.resourceserver.hybris.config;

import com.usta.api.resourceserver.config.AbstractRestTemplateConfig;
import com.usta.api.resourceserver.interceptor.LogRestTemplateInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class HybrisConfig extends AbstractRestTemplateConfig {

    @Bean(name = "hybrisRestTemplate")
    public RestTemplate getHybrisRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
        restTemplate.getInterceptors().add(new LogRestTemplateInterceptor());
        return restTemplate;
    }

}
