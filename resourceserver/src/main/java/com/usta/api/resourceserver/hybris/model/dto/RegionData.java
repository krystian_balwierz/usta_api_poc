package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

@Data
public class RegionData implements java.io.Serializable {


    private String isocode;


    private String isocodeShort;


    private String countryIso;


    private String name;
}
