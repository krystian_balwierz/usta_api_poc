package com.usta.api.resourceserver.hybris.model.dto;


import lombok.Data;

import java.util.Date;

@Data
public class AddressData implements java.io.Serializable {
    private String id;


    private String title;


    private String titleCode;


    private String firstName;


    private String lastName;


    private String companyName;


    private String line1;


    private String line2;


    private String town;


    private RegionData region;


    private String postalCode;


    private String phone;


    private String email;


    private CountryData country;


    private boolean shippingAddress;


    private boolean billingAddress;


    private boolean defaultAddress;


    private boolean visibleInAddressBook;


    private String formattedAddress;


    private String county;


    private String middleName;


    private String addressName;


    private String gender;


    private boolean isPrimaryAddress;


    private String state;


    private Date dateOfBirth;


    private String taDistrictCode;


    private String taSectionCode;


    private String taAddressType;


    private String locationDescription;


    private String line3;


    private String taLine3;


    private USTASectionData taSection;


    private USTADistrictData taDistrict;


    private Double latitude;


    private Double longitude;


    private Double distance;


    private Boolean isValid;


    private String orgAddressStatus;
}
