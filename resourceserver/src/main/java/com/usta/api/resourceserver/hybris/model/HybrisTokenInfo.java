package com.usta.api.resourceserver.hybris.model;

import lombok.Data;

@Data
public class HybrisTokenInfo {
    private String accessToken;
    private String refreshToken;
    private String expiresIn;

    public HybrisTokenInfo(HybrisTokenResponse response) {
        accessToken = response.getAccessToken();
        refreshToken = response.getRefreshToken();
        expiresIn = response.getExpiresIn();
    }
}
