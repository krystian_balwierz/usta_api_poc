package com.usta.api.resourceserver.hybris.model.dto;

import lombok.Data;

@Data
public class USTAOrganizationProviderWsDTO implements java.io.Serializable {


    private String uid;


    private String uaid;


    private String name;


    private AddressWsDTO address;
}
