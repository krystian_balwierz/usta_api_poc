package com.usta.api.resourceserver.hybris.section.controller;

import com.usta.api.resourceserver.hybris.section.model.SectionResponse;
import com.usta.api.resourceserver.hybris.section.service.SectionService;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
class SectionController {
    
    SectionService sectionService;
    
    SectionController(SectionService sectionService) {
        this.sectionService = sectionService;
    }
    
    @GetMapping("getSection")
    String getSection(@CookieValue String accessToken,
                      @RequestParam MultiValueMap<String, String> parameters)
        throws MissingServletRequestParameterException {
    
        SectionResponse response = sectionService.doRequest(accessToken, parameters);
        
        return response.toString();
    }
    
}
