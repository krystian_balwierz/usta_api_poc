package com.usta.api.resourceserver.hybris.organization.model;

import com.usta.api.resourceserver.hybris.model.dto.USTAOrganizationWsDTO;
import lombok.Data;

import java.util.List;

@Data
public class GetOrganizationResponse {
    private List<USTAOrganizationWsDTO> ustaOrganizations;
}
